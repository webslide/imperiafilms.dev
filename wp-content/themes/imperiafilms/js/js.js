$(document).ready(function() {

 //$("body",$("iframe").contents()).append('<div>ES</div>');
  
	$('.call_back').on('click', function() {  // попап сообщения //
		$('.overlay').fadeIn('400');
		$('.messadge').animate({top: "50px"}, 500);
		return false;
	});
	$('div.close').click(function(){
		$('.overlay').fadeOut('400');
		$('.messadge, .zakaz, .quest').animate({top: "-605px"}, 500);
	});
	$('.overlay').click(function(){
		$('.overlay').fadeOut('400');
		$('.messadge, .zakaz, .quest').animate({top: "-605px"}, 500);
	});
	
	$('.cena_zakaz').on('click', function() {  // попап заказа //
		$('.overlay').fadeIn('400');
		$('.zakaz').animate({top: "50px"}, 500);
		return false;
	});
	
	$('.cena_more').on('click', function() {  // попап узнать цену //
		$('.overlay').fadeIn('400');
		$('.quest').animate({top: "50px"}, 500);
		return false;
	});
	
	
	$(window).resize(function(){   // высота от экрана
		 $('.main_bl').height($(window).height() -180); 
	})
	$(document).ready(function(){
		 $('.main_bl').height($(window).height() -180);
	})
	
	
	$('.page_video').height($('.page_video').width() / 1.8);  // соотношение высоты к ширине
	$(window).resize(function(){
		$('.page_video').height($('.page_video').width() / 1.8)
	})
	
	
	$('.portf_images').height($('.portf_images').width() / 1.5);  // соотношение высоты к ширине
	$(window).resize(function(){
		$('.portf_images').height($('.portf_images').width() / 1.5)
	})
	
	
	$('.portf_min').height($('.portf_min').width());  // соотношение высоты к ширине
	$(window).resize(function(){
		$('.portf_min').height($('.portf_min').width())
	})
	
	$('a[href^="/#"]').click(function () { // скролл к блоку
		var el = $(this).attr('href');
		el = el.replace('/', '');
		$('body').animate({
			scrollTop: $(el).offset().top
		}, 1500);
		return false;
	});
	
	
	$('.menu_show').click(function() { // клас доп меню при клике //
		if($('.main_menu').hasClass('menu_active') === false ){
			$('.main_menu').addClass('menu_active');
		}else{
			$('.main_menu').removeClass('menu_active');
		}
	});
	
	$('.in_top').click(function() { // наверх //
		$('body').animate({ scrollTop: 0 }, "slow");		
	});
	
	if($(document).width() <= 530) {  // срабатывание от определенной ширины, перенос блоков //
		$(window).resize(function(){   // высота от экрана
			 $('.main_bl').height($(window).height()); 
		})
		$(document).ready(function(){
			 $('.main_bl').height($(window).height());
		})
	};
	
	
	$('.logo').click(function() { // показать видео
		$('.overlay').fadeIn('400');
		if($('.video').hasClass('media_in') === false ){
			$('.video').addClass('media_in');
		}else{
			$('.video').removeClass('media_in');
		}
	});
	$('.close_media').click(function() { // закрыть блок видео
		if($('.media').hasClass('media_in') === false ){
			$('.media').addClass('media_in');
		}else{
			$('.media').removeClass('media_in');
		}
		$('.overlay').fadeOut('400');
	});
	
	
	if($(document).width() <= 750) {  // срабатывание от определенной ширины, перенос блоков //
		if($('.ceni_bl').hasClass('ceni_hover') === true ){
			$('.ceni_bl').removeClass('ceni_hover');
		}
	};
	
	$('.ceni_bl').click(function() { // добавление класса кнопке при клике //               selector.attr('id' ,'idname')
		$('.ceni_bl').removeClass('ceni_go');  // убрать класс
		$(this).toggleClass('ceni_go')
	});
	
	$(".uslugi_blok").height($(".uslugi_blok").eq(0).width() / 1.4);

	
	

	updateLine();

	$(window).resize(function(){
		$('.gallery_line').height($('.gallery_over').height() / 3.03);  // высота лайнов
		$('.gallery_img').height($('.gallery_line').height() )
		$('.gallery_img').width($('.gallery_img').height()  * 2)
		$('.gallery_img').width($('.gallery_img').height()  * 2);
        $('.gallery_big').width($('.gallery_img').height()  * 3);
        updateLine();
	});
	
	
	$('.close_text').click(function() { // показать описание категории при клике
		if($('.cat_text_slide').hasClass('cat_text_go') === false ){
			$('.cat_text_slide').addClass('cat_text_go');
		}else{
			$('.cat_text_slide').removeClass('cat_text_go');
		}
	});
	
	
	$('.tabs_vid li').click(function() { // показать описание категории при клике
		if($('.tabs_vid').hasClass('tabs_go') === false ){
			$('.tabs_vid').addClass('tabs_go');
		}
		if($('.ceni_vid').hasClass('ceni_go') === false ){
			$('.ceni_vid').addClass('ceni_go');
		}
		if($('.tabs_foto').hasClass('tabs_go2') === true ){
			$('.tabs_foto').removeClass('tabs_go2');
			$('.ceni_foto').removeClass('ceni_go');
		}
	});
	
	$('.tabs_foto li').click(function() { // показать описание категории при клике
		if($('.tabs_foto').hasClass('tabs_go2') === false ){
			$('.tabs_foto').addClass('tabs_go2');
		}
		if($('.ceni_foto').hasClass('ceni_go') === false ){
			$('.ceni_foto').addClass('ceni_go');
		}
		if($('.ceni_vid').hasClass('ceni_go') === true ){
			$('.tabs_vid').removeClass('tabs_go');
			$('.ceni_vid').removeClass('ceni_go');
		}
	});
	
	$('.uslugi_bl .portf_images').height($('.uslugi_bl .portf_images').width() / 2)  // соотношение высоты к ширине
	$(window).resize(function(){
		$('.uslugi_bl .portf_images').height($('.uslugi_bl .portf_images').width() / 2)
	})
	

    updateLine();
	
	

});



	
function updateLine() {
	if($(document).width() > 1024) {  
		$('.gallery_over, .content_over').height($(window).height() -112);  // высота контейнера лайнов
		$('.content_ceni').height($(window).height() -102);  // высота контейнера цен
		$('.gallery_line').height($('.gallery_over').height() / 3.03);  // высота лайнов
		$('.gallery_img').height($('.gallery_line').height() );  // высота превью блоков
		$('.gallery_img').width($('.gallery_img').height()  * 2);  // ширина превью блоков
		$('.gallery_big').width($('.gallery_img').height()  * 3);  // ширина превью блоков
		
		$('.ceni_bl').height($('.content_ceni').height() +10)

		$("#line_1").each(function (i, elem) {
			var countChild = $(elem).find(".portf_images.gallery_img").length;
			var shareWidth = 0;
			//alert($(elem).find(".portf_images.gallery_img").length);
			for (var i = 0; i <= countChild; i++) {
				//alert($(elem).find(".portf_images.gallery_img").width());
				shareWidth += $(elem).find(".portf_images.gallery_img").eq(i).width();
			}
			$("#line_1").width(shareWidth);
		});

		$("#line_2").each(function (i, elem) {
			var countChild = $(elem).find(".portf_images.gallery_img").length;
			var shareWidth = 0;
			//alert($(elem).find(".portf_images.gallery_img").length);
			for (var i = 0; i <= countChild; i++) {
				//alert($(elem).find(".portf_images.gallery_img").width());
				shareWidth += $(elem).find(".portf_images.gallery_img").eq(i).width();
			}
			$("#line_2").width(shareWidth);
		});

		$("#line_3").each(function (i, elem) {
			var countChild = $(elem).find(".portf_images.gallery_img").length;
			var shareWidth = 0;
			//alert($(elem).find(".portf_images.gallery_img").length);
			for (var i = 0; i <= countChild; i++) {
				//alert($(elem).find(".portf_images.gallery_img").width());
				shareWidth += $(elem).find(".portf_images.gallery_img").eq(i).width();
			}
			$("#line_3").width(shareWidth);
		});
	}
}



