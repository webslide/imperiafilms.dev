

<div class="overlay"></div>

<div class="popup zakaz">
    <div class="close"></div>
    <p>Заказать</p>

    <div class="popup_form">
        <form id="form_price_request" method="post" action="">
            <input type="text" name="name_price_request" id="name" placeholder="Ваше имя" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваше имя*')" />
            <input type="text" name="phone_price_request" id="phone" placeholder="Ваш телефон*" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваш телефон*')" />
            <input type="submit" value="Отправить" title=""/>
        </form>
    </div>
</div>

<div class="popup quest">
    <div class="close"></div>
    <p>Заказать</p>

    <div class="popup_form">
        <form id="popup_form_price" method="post" action="">
            <input type="text" name="name_form_price" id="name" placeholder="Ваше имя" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваше имя*')" />
            <input type="text" name="phone_form_price" id="phone" placeholder="Ваш телефон*" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваш телефон*')" />
            <input type="email" name="email_form_price" id="email" placeholder="Ваша почта" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваша почта')" />
            <textarea name="description_task_form_price" placeholder="Опишите проект" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Опишите проект')"></textarea>
            <input type="text" name="link_example_video_form_price" id="link_example_video" placeholder="Пример видео (ссылка)" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Пример видео (ссылка)')" />
            <input type="submit" value="Отправить" title=""/>
        </form>
    </div>
</div>

<div class="popup messadge">
    <div class="close"></div>
    <p>Отправить сообщение</p>

    <div class="popup_form">
        <form id="popup_form_home" method="post" action="">
            <input type="text" name="name_popup_home" id="name" placeholder="Ваше имя" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваше имя*')" />
            <input type="text" name="phone_popup_home" id="phone" required placeholder="Ваш телефон*" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваш телефон*')" />
            <textarea name="message_popup_home" placeholder="Ваш сообщение" onfocus="$(this).attr('placeholder', '')" onblur="$(this).attr('placeholder', 'Ваше сообщение')"></textarea>
            <input type="submit" value="Отправить" title=""/>
        </form>
    </div>
</div>

<?php wp_footer();?>
</body>
</html>
