<?php
add_theme_support('post-thumbnails');
show_admin_bar(false);

add_action('after_setup_theme', function () {
    register_nav_menus(array(
        'topMainMenu' => 'Главное меню (шапка)',
        'portfolioMenu' => 'Меню категорий портфолио'
    ));
});

add_filter('nav_menu_css_class', 'special_nav_class', 10, 3);

function special_nav_class($classes, $item)
{
    if (in_array('current-menu-item', $classes) || (in_array('current-menu-ancestor', $classes))) {
        //current-menu-ancestor
        $classes[] = 'act ';
    }
    return $classes;
}

/* Сообщения */

add_action('wp_ajax_siteWideMessage', 'sendmail');
add_action('wp_ajax_nopriv_siteWideMessage', 'sendmail');

function sendmail()
{
    $admin = get_option('admin_email');

    if(isset($_REQUEST['name_bot_home'])){
        var_dump($_REQUEST['name_bot_home']);
        $subject = "ИмперияFilms | Запрос стоимости";

        $name_bot = htmlspecialchars(trim($_REQUEST['name_bot_home']));
        $mobile_bot = htmlspecialchars(trim($_REQUEST['mobile_bot_home']));
        $task_description_bot = htmlspecialchars(trim($_REQUEST['task_description_bot_home']));
        $message = "Имя: $name_bot \r\n";
        $message .= "Телефон: $mobile_bot \r\n";
        $message .= "Описание задачи: $task_description_bot";
    }else if(isset($_REQUEST['name_popup_home'])){
        $subject = "ИмперияFilms | Заказ";

        $name_popup = htmlspecialchars(trim($_REQUEST['name_popup_home']));
        $phone_popup = htmlspecialchars(trim($_REQUEST['phone_popup_home']));
        $message_popup = htmlspecialchars(trim($_REQUEST['message_popup_home']));

        $message = "Имя: $name_popup \r\n";
        $message .= "Телефон: $phone_popup \r\n";
        $message .= "Описание задачи: $message_popup";
    }
    else if(isset($_REQUEST['name_form_price'])){
        $subject = "ИмперияFilms | Уточнение цены";

        $name_form_price = htmlspecialchars(trim($_REQUEST['name_form_price']));
        $phone_form_price = htmlspecialchars(trim($_REQUEST['phone_form_price']));
        $email_form_price = htmlspecialchars(trim($_REQUEST['email_form_price']));
        $description_task_form_price = htmlspecialchars(trim($_REQUEST['description_task_form_price']));
        $link_example_video_form_price = htmlspecialchars(trim($_REQUEST['link_example_video_form_price']));

        $message = "Имя: $name_form_price \r\n";
        $message .= "Телефон: $phone_form_price \r\n";
        $message .= "E-mail: $email_form_price \r\n";
        $message .= "Описание задачи: $description_task_form_price \r\n";
        $message .= "Ссылка на пример видео: $link_example_video_form_price";
    }else{
        $subject = "ИмперияFilms | Заказ";

        $name_price_request = htmlspecialchars(trim($_REQUEST['name_price_request']));
        $phone_price_request = htmlspecialchars(trim($_REQUEST['phone_price_request']));

        $message = "Имя: $name_price_request \r\n";
        $message .= "Телефон: $phone_price_request \r\n";
    }
    $headers = 'From: ' . $admin . '' . "\r\n" .
        'Reply-To: ' . $admin . '' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

    $mail = wp_mail($admin, $subject, $message, $headers);
    die();
}

function parseYouTubeUrl($url){
    $param = array();
    foreach (explode('&', $url) as $chunk) {
        $param = explode("=", $chunk);
    }
    return $param;
}

function parseVimeoUrl($url){
    //https://player.vimeo.com/video/198811503"
    $param = explode('/', $url);
    return 'https://player.vimeo.com/video/'.$param[3];
}


/* Определение подключения страниц постов */

define('SINGLE_PATH', TEMPLATEPATH . '/single');

add_filter('single_template', 'my_single_template');

function my_single_template($single)
{
    global $wp_query, $post;

    if (file_exists(SINGLE_PATH . '/single-' . $post->ID . '.php')) {
        return SINGLE_PATH . '/single-' . $post->ID . '.php';
    }

    foreach ((array)get_the_category() as $cat) :

        if (file_exists(SINGLE_PATH . '/single-cat-' . $cat->slug . '.php'))
            return SINGLE_PATH . '/single-cat-' . $cat->slug . '.php';

        elseif (file_exists(SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php'))
            return SINGLE_PATH . '/single-cat-' . $cat->term_id . '.php';

    endforeach;

    $wp_query->in_the_loop = true;
    foreach ((array)get_the_tags() as $tag) :
        if (is_object($tag)) {
            if (file_exists(SINGLE_PATH . '/single-tag-' . $tag->slug . '.php'))
                return SINGLE_PATH . '/single-tag-' . $tag->slug . '.php';

            elseif (file_exists(SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php'))
                return SINGLE_PATH . '/single-tag-' . $tag->term_id . '.php';
        }
    endforeach;
    $wp_query->in_the_loop = false;

    if (file_exists(SINGLE_PATH . '/single.php')) {
        return SINGLE_PATH . '/single.php';
    }
    return $single;
}
