<?php get_header();?>
<div class="main_bl">
    <div class="logo">
        <span><img src="<?php bloginfo('template_url')?>/images/logo.png" alt=""/></span>
    </div>

    <div class="slogan">
        <p>Видеопроизводство</p>
        <span>8 (906) 711-82-90</span>
        <div class="call_back">
            ЗАКАЗАТЬ
        </div>
    </div>
</div>

<div class="arr">
    <a href="/#uslugi" title=""></a>
</div>



<div class="title" id="uslugi">
    <p>НАШИ <span>УСЛУГИ</span></p>
</div>
<?php
//id = 22
    $categories_info = get_categories(array(
        'child_of'      => 22,
        //'parent'      => 0,
        'hide_empty'    => 0,
        //'exclude'       => '21',
        'number'        => '0',
        'taxonomy'      => 'category',
        'pad_counts'    => true));

?>
<div class="uslugi_bl">
    <?php
    /*["id"], ["alt"], ["title"], ["caption"], ["description"], ["mime_type"], ["url"], ["width"], ["height"], ["sizes"]:
     *  ["thumbnail"], ["medium"], ["medium_large"], ["large"]
     * */
    foreach ($categories_info as $categories_item) {
        $images = get_field('tile_to_index_our_services', 'category_' . $categories_item->cat_ID);
        echo '<div class="uslugi_blok">';
        echo'<a href="'.get_category_link($categories_item->cat_ID).'" title=""><img src="'.$images['sizes']['large'].'" alt="image"><span>'.$categories_item->cat_name.'</span></a>';
        echo'</div>';
    }
    ?>
</div>




<div class="title">
    <p>НАШИ <span>РАБОТЫ</span></p>
</div>


<div class="portf_cont" id="gallery">
    <div class="uslugi_bl uslugi_vid">
    <?php
        //30 block_our_work
    $posts = get_posts( array(
        'numberposts' => 12,
        'category'    => 30,
        'orderby'     => 'date',
        'order'       => 'DESC',
        'post_type'   => 'post',
        'suppress_filters' => true
    ));
    foreach($posts as $post){ setup_postdata($post);
        echo '<div class="portf_images">   
            <a class="swipebox-video" rel="vimeo" href="'.get_field('link_to_video').'">
                <img src="'.get_field('background_image_tile').'" alt="image">
            </a>
            <div class="portf_text">
                <span>'.$post->post_title.'</span>
                <div class="portf_date">'.get_the_date('Y').'</div>
                <div class="portf_info">
                   '.$post->post_content.'
                </div>
            </div>
        </div>';
    }

    wp_reset_postdata();
    ?>
    </div>

</div>

<div class="main_text">
    <?php
    $main_post = get_post(141,OBJECT);
    ?>
    <h1><?php echo $main_post->post_title; ?></h1>
    <p>
    <?php echo $main_post->post_content; ?></p>
</div>
<?php get_footer('home');?>


