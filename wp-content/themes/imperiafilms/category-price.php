<?php get_header(); ?>
    <div class="content_over content_ceni">
        <?php while (have_posts()) : the_post(); ?>
            <div class="ceni_bl ceni_hover">
                <div class="ceni_img" style="background:url(<?php
                if(get_field('image_for_a_larger_display')){
                   echo get_field('image_for_a_larger_display')['url'];
                }?>) no-repeat top;"></div>
                <div class="ceni_title">
                    <?php the_title();?>
                </div>

                <div class="ceni_cont">
                    <?php the_content();?>
                    <div class="cena">
                        от <?php
                        if(get_field('image_for_a_larger_display')){
                            echo get_field('serv_price');
                        }
                        ?>
                    </div>

                    <div class="cena_more">
                        УТОЧНИТЬ ЦЕНУ
                    </div>
                    <div class="cena_zakaz">
                        ЗАКАЗАТЬ
                    </div>


                </div>

            </div>
        <?php endwhile; ?>
    </div>
<?php get_footer(); ?>