<?php get_header(); ?>
<div class="page_over">
    <div class="page_center">
        <div class="page_content">
            <div class="page_video">
                <?php
                $youtubeUrl = preg_match('/((?:www\.)?youtube\.com|(?:www\.)?youtube-nocookie\.com)\/watch\?v=([a-zA-Z0-9\-_]+)/ ', get_field('link_to_video'));
                $youtubeShortUrl = preg_match('/(?:www\.)?youtu\.be\/([a-zA-Z0-9\-_]+)/', get_field('link_to_video'));
                $vimeoUrl = preg_match('/(?:www\.)?vimeo\.com\/([0-9]*)/', get_field('link_to_video'));
                $iframe = '';
                if ($youtubeUrl || $youtubeShortUrl) {
                    if ($youtubeShortUrl) {
                        $youtubeUrl = $youtubeShortUrl;
                    }
                    $iframe = '<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' . parseYouTubeUrl(get_field('link_to_video'))[1] . '" frameborder="0" allowfullscreen></iframe>';
                } else if ($vimeoUrl) {
                    parseVimeoUrl(get_field('link_to_video'));
                    $iframe = '<iframe width="100%" height="100%" src="' . parseVimeoUrl(get_field('link_to_video')) . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
                }
                echo $iframe;
                ?>
            </div>
            <h1>
                <?php if (have_posts()) : while (have_posts()) :
                the_post();
                the_title();
                ?>
            </h1>
            <div class="page_text">
                <?php the_content(); ?>
            </div>
            <!--
            <nav id="nav-single">
			    <span class="nav-previous">
                    <php previous_post_link('&laquo; %link | ','%title',true, '21'); ?>
			    </span>

                <span class="nav-next">
                    <php next_post_link('%link &raquo; ','%title',true, '21'); ?>
			    </span>
            </nav>
            !-->
            <?php endwhile;
            endif; ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
