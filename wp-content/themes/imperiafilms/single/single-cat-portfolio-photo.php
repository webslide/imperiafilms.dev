<?php get_header(); ?>
    <div class="page_over">
        <div class="page_center">

            <?php if (have_posts()) : while (have_posts()) :
                the_post();
                ?>
                <div class="foto_title">
                    <h1><?php the_title(); ?></h1>
                </div>

                <div class="foto_cont">
                    <?php
                    $images = get_field('photo_gallery');
                    if ($images) {
                        foreach ($images as $image) {
                            echo '    
                                <div class="foto_bl">
                                    <a rel="gallery-2" href="'.$image['url'].'" class="swipebox" title="'.get_the_title().'">
                                        <img src="'.$image['sizes']['large'].'" alt="image">
                                    </a>
                                </div>';
                        }
                    } else {
                        echo '<h1>На данный момент, фотографии не загружены</h1>';
                    }
                    ?>
                </div>

                <div class="page_text">
                    <?php the_content(); ?>
                </div>
            <?php endwhile;
            endif; ?>
        </div>
    </div>
<?php get_footer(); ?>