<?php get_header(); ?>
<?php
global $wp;
$current_url = add_query_arg($wp->query_string, '', home_url($wp->request));
//var_dump($current_url);
?>
    <!-- SCROLL  -->
    <script type="text/javascript">
        // $(function () {
        //     var view = document.getElementById('gal');
        //
        //
        // 	if($(document).width() >= 1024) {
        // 		horwheel(view);
        // 	};
        //
        //     next = true;
        // });


        if ($(document).width() >= 1024) {
            // $(document).on('wheel', function () {
            // 	var maxScroll = $('#container').prop('scrollWidth') - $('#container').width();
            // 	if ($('#container').scrollLeft() == maxScroll && next == true) {
            // 		loadPosts();
            // 		next = false;
            // 		setTimeout(function () {
            // 			next = true;
            // 		}, 1500);
            // 	}
            // });
            (function (doc) {
                var viewport = document.getElementById('viewport');
                if (navigator.userAgent.match(/iPad/i)) {
                    doc.getElementById("viewport").setAttribute("content", "width=1200");
                }
            }(document));

            $(document).ready(function () {
                init();
            });
        }

        function loadPosts() {
            var cat = $('#container').data('cat_id');
            var page = $('#container').data('page');
            $.ajax({
                type: 'POST',
                url: '<?php echo bloginfo('template_url') ?>/ajax-worker.php',
                data: 'category=' + cat + '&page=' + page,
                success: function (data) {
                    $('#container').data('page', page + 1);
                    $('#container').attr('data-page', page + 1);
                    posts = JSON.parse(data);
                    $.each(posts, function (selector) {
                        $.each(this, function () {
                            $('#' + selector).append('<div class="portf_images ' + this['tile-long'] + '">' +
                                '<a href="' + this["single-url"] + '" title="' + this["title"] + '">' +
                                '<img src="' + this["post-image"] + '" alt="image">' +
                                '</a>' +
                                '<div class="portf_text">' +
                                '<span>' + this["title"] + '</span>' +
                                '<div class="portf_date">' + this["date"] + '</div>' +
                                '<div class="portf_info">'
                                + this["excerpt"] +
                                '</div>' +
                                '</div>' +
                                '</div>'
                            );
                        });
                    });
                    updateLine();
                }
            });
        }
    </script>
    <div class="gallery_over" id="container" data-cat_id="<?php echo $cat ?>" data-page="1">
        <div class="gallery_cont" id="pass">
            <div class="gallery_menu"
                 style="background:url(<?php bloginfo('template_url'); ?>/images/gallery_menu.jpg) no-repeat top;">
                <div class="cat_text_slide">
                    <div class="cat_text">
                        <!-- <h1><?php echo get_queried_object()->name; ?></h1> -->
                        <table>
							 <tr>
								 <td><?php echo category_description(); ?></td>
							 </tr>
						 </table>
                    </div>
                    <div class="close_text"></div>
                </div>
                <script>
                    $(document).ready(function() {

                        var currUrl = window.location.pathname;
                        currUrl = currUrl.split("/");
                        if(currUrl[currUrl.length-2] == 'portfolio')
                            $('.left-menu li#portfolio').addClass("act");
                        $('.left-menu li').each(function () {
                            if ($(this).attr("id") == currUrl[currUrl.length-2])
                                $(this).addClass("act");
                        })
                    });
                </script>
                <ul class="left-menu">
                    <!-- <li id="portfolio" > <a href="<?php echo get_category_link('22'); ?>">ВСЁ</a></li> -->
                    <?php
                    //echo '<li id="portfolio" ><a href="'.get_category_link(22).'">ВСЁ</a></li>';
                    $categories = get_categories(array(
                            'type' => 'post',
                            'parent' => 22,
                            'orderby' => 'name',
                      		'hide_empty'   => 0,
                            'order' => 'ASC')
                    );
                    //<li id="portfolio"><a href='<?php echo get_home_url().'/category/portfolio''>ВСЁ</a></li>
                    foreach ($categories as $itemCat) {
                        echo '<li id="'.$itemCat->slug.'"><a href='.get_category_link($itemCat->cat_ID).'>'.mb_strtoupper($itemCat->cat_name).'</a></li>';
                    }
                    ?>
                </ul>
            </div>
            <div class="gallery_lines">
                <?php
                $getPosts = array(
                    "line1" => array(),
                    "line2" => array(),
                    "line3" => array()
                );
                ?>
                <?php $i = 1;
                if (have_posts()) : while (have_posts()) : the_post();
                    $getPosts['line' . $i]["post-" . get_the_ID()]['title'] = get_the_title();
                    $getPosts['line' . $i]["post-" . get_the_ID()]['date'] = get_the_date('Y');
                    $getPosts['line' . $i]["post-" . get_the_ID()]['excerpt'] = get_the_excerpt() ;
                    $getPosts['line' . $i]["post-" . get_the_ID()]['single-url'] = get_permalink();
                    $image = get_field('background_image_tile');
                    if (!empty($image)) {
                        $getPosts['line' . $i]["post-" . get_the_ID()]['post-image'] = $image;
                        //echo '<img src="' . $image['url'] . '" alt=""/>';
                    }
                    if (get_field('longer_tile')) {
                        $getPosts['line' . $i]["post-" . get_the_ID()]['tile-long'] = 'gallery_img gallery_big';
                    } else {
                        $getPosts['line' . $i]["post-" . get_the_ID()]['tile-long'] = 'gallery_img';
                    }
                    if (get_field('link_to_video')) {
                        $getPosts['line' . $i]["post-" . get_the_ID()]['video-link'] = get_field('link_to_video');
                    }
                    if ($i == 3)
                        $i = 1;
                    else
                        $i++;
                endwhile;
                endif;
                wp_reset_query();
                wp_reset_postdata();
                ?>
                <div id="line_1" class="gallery_line">
                    <?php foreach ($getPosts["line1"] as $postitem) {
                        echo '<div class="portf_images ' . $postitem["tile-long"] . '">';
                        echo '<a href="' . $postitem["single-url"] . '" title="' . $postitem["title"] . '">
							        <img src="' . $postitem["post-image"] . '" alt="image">
						          </a>';
                        echo '<div class="portf_text">';
                        echo '<span>' . $postitem["title"] . '</span>
							        <div class="portf_date">' . $postitem["date"] . '</div>
							        <div class="portf_info">
								    ' . $postitem["excerpt"] . '
							        </div>
						            </div>';
                        echo '</div>';
                    } ?>
                </div>
                <div id="line_2" class="gallery_line">
                    <?php foreach ($getPosts["line2"] as $postitem) {
                        echo '<div class="portf_images ' . $postitem["tile-long"] . '">';
                        echo '<a href="' . $postitem["single-url"] . '" title="' . $postitem["title"] . '">
							        <img src="' . $postitem["post-image"] . '" alt="image">
						          </a>';
                        echo '<div class="portf_text">';
                        echo '<span>' . $postitem["title"] . '</span>
							        <div class="portf_date">' . $postitem["date"] . '</div>
							        <div class="portf_info">
								    ' . $postitem["excerpt"] . '
							        </div>
						            </div>';
                        echo '</div>';
                    } ?>
                </div>
                <div id="line_3" class="gallery_line">
                    <?php foreach ($getPosts["line3"] as $postitem) {
                        echo '<div class="portf_images ' . $postitem["tile-long"] . '">';
                        //<a class="swipebox-video" rel="vimeo" href="http://vimeo.com/29193046">My Videos</a>
                        echo '<a href="' . $postitem["single-url"] . '" title="' . $postitem["title"] . '">
                        	        <img src="' . $postitem["post-image"] . '" alt="image">
						          </a>';
                        echo '<div class="portf_text">';
                        echo '<span>' . $postitem["title"] . '</span>
							        <div class="portf_date">' . $postitem["date"] . '</div>
							        <div class="portf_info">
								    ' . $postitem["excerpt"] . '
							        </div>
						            </div>';
                        echo '</div>';
                    } ?>
                </div>
            </div>

        </div>
    </div>

    </div>

    <div class="overlay"></div>
<?php get_footer(); ?>
