<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link href="<?php bloginfo('template_url')?>/favicon.ico" rel="shortcut icon" type="icon"/>

    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url')?>/style.css"/>

    <title><?php echo wp_get_document_title();?></title>

    <!--БИБЛИОТЕКА-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <!-- JS -->
    <script src="<?php bloginfo('template_url')?>/js/js.js"></script>
    <!-- <script src="<?php bloginfo('template_url')?>/js/horwheel.js"></script> -->
	
	<!-- ГОРИЗОНТАЛЬНЫЙ СКРОЛЛ -->
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/scroll/TweenMax.min.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/scroll/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/scroll/cm.js"></script>
	<script type="text/javascript">
		// (function (doc) {
		// 	var viewport = document.getElementById('viewport');
		// 	if (navigator.userAgent.match(/iPad/i)) {
		// 		doc.getElementById("viewport").setAttribute("content", "width=1200");
		// 	}
		// }(document));
        // 
		// $(document).ready(function () {
		// 	init();
		// });
	</script>

    <!-- ГАЛЕРЕЯ -->
    <script src="<?php bloginfo('template_url')?>/js/jquery.swipebox.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            /* Basic Gallery */
            $( '.swipebox' ).swipebox();

            //Сообщения
            $('#form_bot_home').submit(function (e) {
                funcSendMessage($(this).attr("id"));
                return false;
            });
            $('#popup_form_home').submit(function (e) {
                funcSendMessage($(this).attr("id"));
                return false;
            });
            $('#popup_form_price').submit(function (e) {
                funcSendMessage($(this).attr("id"));
                return false;
            });

            $('#form_price_request').submit(function (e) {
                funcSendMessage($(this).attr("id"));
                return false;
            });

            function funcSendMessage(form) {
                var tmp = 'form#'+form;
                var data = $(tmp).serialize() + '&action=siteWideMessage';
                $.post('<?php echo admin_url('admin-ajax.php');?>', data, function () {
                    alert('Спасибо, Ваше сообщение было отправлено!');
                    $(tmp).trigger('reset');
                    setTimeout(function () {
                        $('.close').click();
                    }, 2000);
                });
            }


        });

    </script>

    <?php wp_head(); ?>
</head>
<body <?php body_class( 'myclass yourclass' ); ?>>
<div class="over">
    <div class="header">
        <div class="name">
            <a href="<?php echo get_home_url(); ?>">ИМПЕРИЯ<span>FILMS</span></a>
        </div>
        <div class="main_menu">
			 <span class="menu_show">
				<span class="line1"></span>
				<span class="line2"></span>
				<span class="line3"></span>
				<span class="line4"></span>
			 </span>
            <?php wp_nav_menu(array('theme_location' => 'topMainMenu')); ?>
        </div>
    </div>