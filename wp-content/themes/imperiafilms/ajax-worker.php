<?php
require($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wp;
$i = 1;
$page = (int)$_REQUEST['page'] + 1;
$cat = $_REQUEST['category'];

$getPosts = array(
    "line_1" => array(),
    "line_2" => array(),
    "line_3" => array()
);

$args = array(
    'paged' => $page,
    'cat' => $cat,
);
query_posts($args);
if (have_posts()) : while (have_posts()) : the_post();
    $getPosts['line_' . $i]["post-" . get_the_ID()]['title'] = get_the_title();
    $getPosts['line_' . $i]["post-" . get_the_ID()]['date'] = get_the_date('Y');
    $getPosts['line' . $i]["post-" . get_the_ID()]['excerpt'] = get_the_excerpt();
    $getPosts['line' . $i]["post-" . get_the_ID()]['single-url'] = get_permalink();
    $image = get_field('background_image_tile');
    if (!empty($image)) {
        $getPosts['line_' . $i]["post-" . get_the_ID()]['post-image'] = $image;
        //echo '<img src="' . $image['url'] . '" alt=""/>';
    }
    if (get_field('longer_tile')) {
        $getPosts['line_' . $i]["post-" . get_the_ID()]['tile-long'] = 'gallery_img gallery_big';
    } else {
        $getPosts['line_' . $i]["post-" . get_the_ID()]['tile-long'] = 'gallery_img';
    }
    if (get_field('link_to_video')) {
        $getPosts['line_' . $i]["post-" . get_the_ID()]['video-link'] = get_field('link_to_video');
    }
    if ($i == 3)
        $i = 1;
    else
        $i++;
endwhile;
endif;

echo json_encode($getPosts);
